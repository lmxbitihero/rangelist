class RangeList
  def initialize
    @items = []
  end

  def add(range)
    new_item = RangeItem.new(range)

    @items.each_with_index{|current_item, i|
      position = new_item.position_of(current_item) 
      if position == "before"
        @items.insert(i, new_item)
        return
      elsif position == "merge"
        current_item.value[0] = [current_item.value[0], new_item.value[0]].min
        current_item.value[1] = [current_item.value[1], new_item.value[1]].max
        fix_add_item(i)
        return
      end
    }

    @items << new_item
  end

  def remove(range)
    remove_from(range, 0)
  end

  def to_s
    @items.map{|item| "[#{item.value[0]}, #{item.value[1]})"}.join(" ")
  end

  def print
    puts to_s
  end

private
  def fix_add_item(index)
    current_item = @items[index]
    next_item    = @items[index+1]

    return unless current_item && next_item

    if current_item.value[1] >= next_item.value[0]
      current_item.value[1] = [current_item.value[1], next_item.value[1]].max
      @items.delete_at(i+1)
      fix_add_item(index)
    end
  end

  def remove_from(range, index)
    return if range[0] == range[1]
    remove_range = RangeItem.new(range)

    index.upto(@items.size-1) do |i|
      current_item = @items[i]
      if remove_range.included_in?(current_item)
        @items.delete_at(i)
        @items.insert(i, RangeItem.new([current_item.value[0], range[0]]))
        @items.insert(i + 1, RangeItem.new([range[1], current_item.value[1]]))
        return
      elsif remove_range.left_cover?(current_item)
        current_item.value[0] = range[1]
        return
      elsif remove_range.right_cover?(current_item)
        new_remove_range = [current_item.value[1], range[1]]
        remove_from(new_remove_range, i+1)
        
        if current_item.value[0] > remove_range.value[0]
          @items.delete_at(i) 
        else
          current_item.value[1] = remove_range.value[0]
        end
        return
      end
    end
  end
end

class RangeItem
  attr_accessor :value

  def initialize(range)
    valid_param(range)
    @value = range
  end

  def position_of(other)
    range_item_only(other)

    if @value.last < other.value.first
      return "before"
    elsif @value.first > other.value.last
      return "after"
    else
      return "merge"
    end
  end

  def included_in?(other)
    range_item_only(other)

    @value[0] > other.value[0] && @value[1] < other.value[1]
  end

  def left_cover?(other)
    range_item_only(other)

    @value[0] <= other.value[0] && @value[1] < other.value[1]
  end

  def right_cover?(other)
    range_item_only(other)

    @value[1] >= other.value[0] && @value[0] < other.value[1]
  end
private
  def range_item_only(other)
    raise AcceptRangeItemError, "Only accept RangeItem class" if other.class != RangeItem
  end
  
  def valid_param(range)
    if range.class != Array || range.size != 2
      raise InvalidRangeError, "Invalid range class #{range}, it must by an Array with 2 integer items"
    end

    if range[0].class != Integer || range[1].class != Integer
      raise ErrInvalidRange, "Invalid range class [#{range[0]}, #{range[1]}], it must by an Array with 2 integer items"
    end

    if range[0] > range[1]
      raise ErrInvalidRange, "Invalid range value, #{range[1]} must greater or equal than #{range[0]}"
    end

    return true
  end
end