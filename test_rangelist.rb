require "test/unit"

require './rangelist.rb'

class TestRangeList < Test::Unit::TestCase
  def setup
    @rl = RangeList.new
  end

  def test_add_remove
    @rl.add([1, 5]) 
    assert @rl.to_s == "[1, 5)"

    @rl.add([10, 20]) 
    assert @rl.to_s == "[1, 5) [10, 20)"

    @rl.add([20, 20]) 
    assert @rl.to_s == "[1, 5) [10, 20)"

    @rl.add([20, 21]) 
    assert @rl.to_s == "[1, 5) [10, 21)"

    @rl.add([2, 4]) 
    assert @rl.to_s == "[1, 5) [10, 21)"

    @rl.add([3, 8]) 
    assert @rl.to_s == "[1, 8) [10, 21)"

    @rl.remove([10, 10]) 
    assert @rl.to_s == "[1, 8) [10, 21)"

    @rl.remove([10, 11]) 
    assert @rl.to_s == "[1, 8) [11, 21)"

    @rl.remove([15, 17]) 
    assert @rl.to_s == "[1, 8) [11, 15) [17, 21)"

    @rl.remove([3, 19]) 
    assert @rl.to_s == "[1, 3) [19, 21)"

    @rl.add([1, 1]) 
    assert @rl.to_s == "[1, 3) [19, 21)"

    @rl.add([3, 4]) 
    assert @rl.to_s == "[1, 4) [19, 21)"

    @rl.add([18, 23]) 
    assert @rl.to_s == "[1, 4) [18, 23)"

    @rl.add([17, 20]) 
    assert @rl.to_s == "[1, 4) [17, 23)"

    @rl.add([7, 10]) 
    assert @rl.to_s == "[1, 4) [7, 10) [17, 23)"

    @rl.remove([7, 7]) 
    assert @rl.to_s == "[1, 4) [7, 10) [17, 23)"

    @rl.remove([7, 8]) 
    assert @rl.to_s == "[1, 4) [8, 10) [17, 23)"

    @rl.remove([10, 10]) 
    assert @rl.to_s == "[1, 4) [8, 10) [17, 23)"

    @rl.remove([9, 10]) 
    assert @rl.to_s == "[1, 4) [8, 9) [17, 23)"
  end
end